//
//  SignInSignInViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/3/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import MBProgressHUD

class SignInSignInViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var passwordConfirmationTxt: UITextField!
    @IBOutlet weak var createAccountBtn: UIButton!
    @IBOutlet weak var badEmailLb: UIView!
    @IBOutlet weak var bacPasswordLb: UIView!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var badPhone: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createAccountBtn.roundCorners(radius: 20)
        hideKeyboardWhenTappedAround()
        nameTxt.delegate = self
        emailTxt.delegate = self
        emailTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passwordTxt.delegate = self
        passwordConfirmationTxt.delegate = self
        passwordConfirmationTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        phoneTxt.delegate = self
        phoneTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        
    }
    @IBAction func back(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createAccount(_ sender: Any) {
        
        if( passwordTxt.text != nil &&
            passwordTxt.text?.isEmpty == false &&
            passwordConfirmationTxt.text != nil &&
            passwordConfirmationTxt.text?.isEmpty == false &&
            nameTxt.text != nil && nameTxt.text?.isEmpty == false &&
            emailTxt.text != nil && emailTxt.text?.isEmpty == false &&
            phoneTxt.text != nil && phoneTxt.text?.isEmpty == false) {
            
            if(badEmailLb.isHidden && bacPasswordLb.isHidden && badPhone.isHidden){
                MBProgressHUD.showAdded(to: self.view, animated: true)
                Auth.auth().createUser(withEmail: emailTxt.text!, password: passwordTxt.text!) { (authResult, error) in
                    if let er = error  {
                        MBProgressHUD.hide(for: self.view, animated: true)
                        print(er.localizedDescription)
                        if (er.localizedDescription == "The email address is already in use by another account.") {
                            let alert = UIAlertController(title: "Alert", message: "The email address is already in use by another account.", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                switch action.style{
                                case .default:
                                    print("default")
                                    
                                case .cancel:
                                    print("cancel")
                                    
                                case .destructive:
                                    print("destructive")
                                    
                                    
                                }}))
                            self.present(alert, animated: true, completion: nil)
                            return
                        }
                        return
                    }
                    
                    if let user = authResult?.user  {
                     print(user.uid)
                        let  newUser  = User(user: user)
                        newUser.name = self.nameTxt.text
                        newUser.phone = self.phoneTxt.text!
                        newUser.save(route: "users")
                        MBProgressHUD.hide(for: self.view, animated: true)
                        self.dismiss(animated: true, completion: nil)
                        
                    }
                    
                    
                    
                    
                }
            }
        }
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == emailTxt {
            if let text = textField.text {
                if ( !isValidEmail(testStr: text )){
                    badEmailLb.isHidden = false
                    
                    
                }else{
                    badEmailLb.isHidden = true
                    
                }
            }
            
        }
        if textField == passwordConfirmationTxt {
            if let text = textField.text {
                if ( passwordTxt.text != text){
                    bacPasswordLb.isHidden = false
                    
                }else{
                    bacPasswordLb.isHidden = true
                    
                }
            }
            
        }
        if textField == phoneTxt {
            if let text = textField.text {
                if ( !isValidPhone(value: text)){
                    badPhone.isHidden = false
                    
                    
                }else{
                    badPhone.isHidden = true
                    
                }
            }
            
        }
    }
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func isValidPhone(value: String) -> Bool {
        let PHONE_REGEX = "^((\\+)|(00))[0-9]{6,14}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result =  phoneTest.evaluate(with: "+57" + value)
        return result
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 30
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
