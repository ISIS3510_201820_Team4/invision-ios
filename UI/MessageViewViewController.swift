//
//  MessageViewViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 11/13/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class MessageViewViewController: UIViewController {

    @IBOutlet weak var labelInfo: UILabel!
    
    @IBOutlet weak var returnBtn: UIButton!
    
    @IBOutlet weak var viewTimer: UIView!
    @IBOutlet weak var labelTimer: UILabel!
    
    @IBOutlet weak var btnReturn: UIButton!
    var color: UIColor?
    var message: String?
    var nv: UINavigationController?
    var vc: UIViewController?
    var circleOpen = false
    var emergency = false
    
    var seconds = 30 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let c = color {
            
            self.view.backgroundColor = c
            self.returnBtn.tintColor = c
            
        }

        if let m = message {
            self.labelInfo.text = m
        }
        
        if (circleOpen){
            viewTimer.isHidden = false
            runTimer()
        }else {
            btnReturn.isHidden = false
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        returnBtn.roundCorners(radius: returnBtn.frame.size.height / 2)
        returnBtn.addCancelShadow()
        returnBtn.layer.shadowRadius = returnBtn.frame.size.height / 2
    }
    
    @IBAction func returnAction(_ sender: Any) {
        
        if(emergency){
            K.Us.currentCircle?.emergency = false
            K.Us.currentCircle?.save(route: "circles")
        }
        
        if nv != nil {
            nv!.popToRootViewController(animated: false)
        }
        
           self.dismiss(animated: true, completion: nil)

    }
    

    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if seconds > 0 {
            seconds -= 1     //This will decrement(count down)the seconds.
            labelTimer.text = "\(seconds)"//This will update the label.
        }
        else {
            labelTimer.text = "Circle locked"
            K.Us.currentCircle?.open = false
            K.Us.currentCircle?.save(route: "circles")
            timer.invalidate()
            _ = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.dis), userInfo: nil, repeats: false)
            
        }
    }
    
    @objc func dis()
    {
        self.dismiss(animated: true, completion: nil)
    }
    
}
