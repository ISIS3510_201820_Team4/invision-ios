//
//  User.swift
//  DocDocPatient
//
//  Created by juan esteban chaparro on 21/10/17.
//  Copyright © 2017 Tres Astronautas. All rights reserved.
//

import Foundation
import Firebase

class User: Object {
    
    static let collectionName = "users"
    
    
    class func withID(id: String, callback: @escaping (_ s: User?)->Void) {
        K.FireStore.ref().collection(collectionName).document(id).getDocument { (document, error) in
            if error != nil {
                print(error!.localizedDescription)
            } else if document != nil && document!.exists {
                if let documentData = document?.data() {
                    callback(User(documentData))
                } else {
                    callback(nil)
                }
            } else {
                callback(nil)
            }
        }
    }
    
    class func withEmail(email: String, callback: @escaping (_ s: [User?])->Void) {
         var users:[User] = []
        K.FireStore.ref().collection(collectionName).whereField("email", isEqualTo: email).getDocuments() { (querySnapshot, err) in
            
            if let err = err {
                print("Error getting documents: \(err)")
            } else if querySnapshot?.isEmpty == false  {
                for document in querySnapshot!.documents {
                    if let documentData = document.data() as [String:Any]? {
                        users.append(User(documentData))
                    }
                }
                callback(users)
            }else {
                callback(users)
            }
            
            }
    }
    
    
    public override init(_ dict: [String: Any]){
        super.init(dict)
        
        if let name = dict["name"] as? String{
            self.name = name
        }
        if let joined = dict["joined"] as? Timestamp {
            self.joined =  joined.dateValue() as NSDate
        }
        if let email = dict["email"] as? String {
            self.email = email
        }
        if let phone = dict["phone"] as? String {
            self.phone = phone
        }
        
        
        
    }
    
    
    public convenience init(user: Firebase.User) {
        var dict = ["name": user.displayName, "id": user.uid ]
        
        if let pp = user.photoURL {
            dict["photo"] = pp.absoluteString
        }
        if let phone = user.phoneNumber {
            
            dict["phone"] = phone
        }
        if let email = user.email {
            
            dict["email"] = email
        }
        self.init(dict as Any as! [String : Any])
    }
    
    override func save(route: String) {
        //if (self.uid != getCurrentUserUid()) {
        //  return
        // }
        
        if self.joined != nil {
            originalDictionary["joined"] = self.joined
        }
        if self.name != nil {
            originalDictionary["name"] = self.name
        }
        
        if self.email != nil {
            originalDictionary["email"] = self.email
        }
        if self.phone != nil {
            originalDictionary["phone"] = self.phone
        }
        
        
        
        super.save(route: User.collectionName)
    }
    
    
    public func prepareForSave() -> [String: Any] {
        //if (self.uid != getCurrentUserUid()) {
        //  return
        // }
        if self.name != nil {
            originalDictionary["name"] = self.name
        }
        if self.joined != nil {
            originalDictionary["joined"] = self.joined
        }
        
        if self.email != nil {
            originalDictionary["email"] = self.email
        }
        if self.phone != nil {
            originalDictionary["phone"] = self.phone
        }
        
        
        
        return originalDictionary
    }
    
    public func getCircles() -> [UserCircle]? {
        var add_participants:[UserCircle] = []
        if let participants = originalDictionary["circles"] {
            
            if let addDict = participants as? [String:AnyObject] {
                for (_, pay) in addDict {
                    if let payDict = pay as? [String:AnyObject] {
                        add_participants.append(UserCircle(payDict))
                    }
                }
                return add_participants
            }
            if let addArray = participants as? [[String:AnyObject]] {
                for pay in addArray {
                    add_participants.append(UserCircle(pay))
                }
                return add_participants
            }
        }
        return nil
    }
    
    public func addCircle(circle : UserCircle){
        var allDictionaries : [Any] = []
        if let pl = getCircles(){
            
            for p in pl{
                
                allDictionaries.append(p.prepareForSave() as Any)
            }
        }
        
        allDictionaries.append(circle.prepareForSave() as Any)
        originalDictionary["circles"] = allDictionaries
        
        save(route: User.collectionName)
        
    }
    
    public func saveNotificationToken(token: String) {
        var lock = false
        for i in getTokens()! {
            if(i == token){
                lock = true
            }
        }
        if(!lock){
            addToken(token: token)
        }
        
    }
    
    public func addToken(token: String) {
        var allDictionaries: [String] = []
        allDictionaries.append(token)
        
        let defaults = UserDefaults.standard
        let tk = defaults.string(forKey: "deviceToken")
        
        for pl in getTokens()!{
            
            if tk != nil {
                
                if pl != tk {
                    allDictionaries.append(pl)
                }
            }
            else{
                allDictionaries.append(pl)
            }
        }
        originalDictionary["tokens"] = allDictionaries
        
        self.save(route:"users")
        
    }
    
    public func getTokens() ->[String]?{
        
        var add_tokens:[String] = []
        if let tokens = originalDictionary["tokens"] {
            if let addDict = tokens as? [String] {
                for pay in addDict {
                    if let payDict = pay as String? {
                        add_tokens.append(payDict)
                    }
                }
                return add_tokens
            }
            
        }
        return []
        
    }
    
    
    var name: String?
    var joined: NSDate?
    var email: String?
    var phone: String?
    
    
    
    
    
    
}
