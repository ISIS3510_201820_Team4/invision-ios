//
//  Event.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/7/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import Foundation
import Firebase

class Event: Object {
    
    static let collectionName = "history"
    
    
//    class func withID(id: String, callback: @escaping (_ s: CircleObject?)->Void) {
//        K.FireStore.ref().collection(collectionName).document(id).getDocument { (document, error) in
//            if error != nil {
//                print(error!.localizedDescription)
//            } else if document != nil && document!.exists {
//                if let documentData = document?.data() {
//                    callback(CircleObject(documentData))
//                } else {
//                    callback(nil)
//                }
//            } else {
//                callback(nil)
//            }
//        }
//    }
//
    
    
    public override init(_ dict: [String: Any]){
        super.init(dict)
        
        if let action = dict["action"] as? NSNumber{
            self.action = action
        }
        if let name = dict["name"] as? String {
            self.name =  name
        }
        if let date = dict["date"] as? NSDate {
            self.date =  date
        }
        if let objectType = dict["objectType"] as? NSNumber {
            self.objectType = objectType
        }
        
        
    }
    
    
    override func save(route: String) {
        
        
        if self.date != nil {
            originalDictionary["date"] = self.date
        }
        if self.action != nil {
            originalDictionary["action"] = self.action
        }
        if self.objectType != nil {
            originalDictionary["objectType"] = self.objectType
        }
        if self.name != nil {
            originalDictionary["name"] = self.name
        }
        
        
        
        super.save(route: route)
    }
    
    
    public func prepareForSave() -> [String: Any] {
        
        if self.date != nil {
            originalDictionary["date"] = self.date
        }
        if self.action != nil {
            originalDictionary["action"] = self.action
        }
        if self.objectType != nil {
            originalDictionary["objectType"] = self.objectType
        }
        if self.name != nil {
            originalDictionary["name"] = self.name
        }
        
        
        
        
        return originalDictionary
    }
    
    
    var date: NSDate?
    var name : String?
    var action: NSNumber?
    var objectType: NSNumber?
    
    
    
    
    
    
}
