//
//  UserCircle.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 10/7/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import Foundation
import Firebase

class UserParticipant: Object {
    
    
    public override init(_ dict: [String: Any]){
        super.init(dict)
        
        if let name = dict["name"] as? String{
            self.name = name
        }
        if let phone = dict["phone"] as? String{
            self.phone = phone
        }
        if let ref = dict["ref"] as? DocumentReference {
            self.ref =  ref
        }
        if let objectID = dict["objectID"] as? String {
            self.objectID = objectID
        }
    }
    
    
    public func prepareForSave() -> [String: Any] {
        
        
        if self.ref != nil {
            originalDictionary["ref"] = self.ref
        }
        if self.phone != nil {
            originalDictionary["phone"] = self.phone
        }
        if self.name != nil {
            originalDictionary["name"] = self.name
        }
        if self.objectID != nil {
            originalDictionary["objectID"] = self.objectID
        }
        
        
        
        return originalDictionary
    }
    
    
    
    var ref: DocumentReference?
    var name : String?
    var objectID: String?
    var phone: String?
    
    
    
    
    
    
}


