//
//  UserActionViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 11/2/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class UserActionViewController: UIViewController {
    
     var userSearch: User?
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var nameLb: UILabel!
    @IBOutlet weak var emailLb: UILabel!
    @IBOutlet weak var phoneLb: UILabel!
    @IBOutlet weak var addToCircle: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        titleView.textColor = K.UI.main_color
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if  userSearch != nil {
            nameLb.text = userSearch?.name
            emailLb.text = userSearch?.email
            phoneLb.text = userSearch?.phone
        }
    }
    
    override func viewDidLayoutSubviews() {
        
        addToCircle.roundCorners(radius: addToCircle.frame.height / 2)
        addToCircle.addCancelShadow()
        addToCircle.layer.shadowRadius = addToCircle.frame.size.height / 2
    }
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addUser(_ sender: Any) {
        if userSearch != nil {
            
            if (K.Us.currentCircle != nil){
                let part = UserParticipant([:])
                part.name = userSearch?.name
                
                part.phone = userSearch?.phone
                if let id = userSearch?.uid {
                    if (K.Us.currentCircle != nil ){
                        
                        if ( !(K.Us.currentCircle?.searchParticipant(uid: id))!){
                            part.ref = K.FireStore.ref().collection("users").document(id)
                            part.objectID = id
                            K.Us.currentCircle?.addParticipant(participant: part)
                            K.Us.currentCircle?.save(route: "circles")
                            
                            let cir = UserCircle([:])
                            cir.name = K.Us.currentCircle?.name
                            if let u = K.Us.currentCircle?.uid {
                                cir.objectID = u
                                cir.ref = K.FireStore.ref().collection("circles").document(u)
                                userSearch?.addCircle(circle: cir)
                                
                                if (userSearch != nil){
                                    let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "MessageViewViewController") as! MessageViewViewController
                                    if let nam = userSearch!.name {
                                        vc.message = "User \(nam) added to circle."
                                    }else{
                                        vc.message = "User Unknow added to circle."
                                    }
                                    
                                    vc.color = K.UI.main_color
                                    vc.nv = self.navigationController
                                    self.present(vc, animated: true, completion: nil)
                                }
                                
                                
                            }
                        }else{
                            
                            if (userSearch != nil){
                                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "MessageViewViewController") as! MessageViewViewController
                                if let nam = userSearch!.name {
                                     vc.message = "User \(nam) is already part of the circle."
                                }else{
                                     vc.message = "User Unknow is already part of the circle."
                                }
                                
                                vc.color = K.UI.main_color
                                vc.nv = self.navigationController
                                self.present(vc, animated: true, completion: nil)
                            }
                            
                           
                           
                        }
                    }
                   
                }
                
                
                
            }
            
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
