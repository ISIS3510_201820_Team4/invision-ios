//
//  FindUserEmailViewController.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 11/2/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit
import FirebaseAuth

class FindUserEmailViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var badEmail: UILabel!
    
     var validate = false;
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        titleView.textColor = K.UI.main_color
        emailTxt.delegate = self
        emailTxt.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillLayoutSubviews() {
        searchBtn.roundCorners(radius: searchBtn.frame.height / 2)
        searchBtn.addCancelShadow()
        searchBtn.layer.shadowRadius = searchBtn.frame.size.height / 2
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == emailTxt {
            if let text = textField.text {
                if ( !isValidEmail(testStr: text )){
                    badEmail.isHidden = false
                    validate = false
                    
                }else{
                    badEmail.isHidden = true
                    validate = true
                }
            }
            
        }
    }
    
    @IBAction func back(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchUser(_ sender: Any) {
        if(validate){
            User.withEmail(email: emailTxt.text!) { (users) in
                if users.count > 0 {
                        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "UserActionViewController") as! UserActionViewController
                        vc.userSearch = users[0]
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                   
                    
                }else {
                    let alert = UIAlertController(title: "Bad ID", message: "We dont have a user with email \( self.emailTxt.text! ).", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        switch action.style{
                        case .default:
                            self.emailTxt.text = ""
                            
                        case .cancel:
                            print("cancel")
                            
                        case .destructive:
                            print("destructive")
                            
                            
                        }}))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let count = text.count + string.count - range.length
        return count <= 30
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
