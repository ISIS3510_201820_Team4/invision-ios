//
//  HistoryCollectionViewCell.swift
//  SmartHome
//
//  Created by juan esteban  chaparro machete on 9/11/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import UIKit

class HistoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var iconImg: UIImageView!
    @IBOutlet weak var mainTitleLb: UILabel!
    
    @IBOutlet weak var dateLb: UILabel!
    @IBOutlet weak var actionLb: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.mainView.layoutIfNeeded() // Add this line
        iconImg.circleImage()
        mainView.roundCorners(radius: 4.0)
        mainView.mainshadow(color: K.UI.shadow_color)
        
    }
    
}
