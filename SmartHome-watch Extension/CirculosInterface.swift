//
//  CirculosInterface.swift
//  SmartHome-watch Extension
//
//  Created by Andrés on 11/26/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import WatchKit
import Foundation

import WatchConnectivity


class CirculosInterface: WKInterfaceController, WCSessionDelegate {
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    var circulos : [String] = []
    
    @IBOutlet weak var tablaCirculos: WKInterfaceTable!
    
    let session = WCSession.default
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        NotificationCenter.default.addObserver(self, selector: #selector(didReceivePhoneData), name: NSNotification.Name(rawValue: "receivedPhoneData"), object: nil)
        
        self.tablaCirculos.setNumberOfRows(circulos.count, withRowType: "CirculosRow")
        
        for index in 0..<circulos.count{
            let row = self.tablaCirculos.rowController(at: index) as! CirculosRow
            
            row.CirculoLabel.setText(circulos[index])
        }
    }
    
    @objc func didReceivePhoneData(info:NSNotification){
        let msg = info.userInfo!
        self.circulos.append((msg["msg"] as? String)!)
    }
    
    override func table(_ table: WKInterfaceTable, didSelectRowAt rowIndex: Int) {
        self.pushController(withName: "CirculoInterface", context: circulos[rowIndex])
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
