//
//  ObjetoInterface.swift
//  SmartHome-watch Extension
//
//  Created by Andrés on 11/26/18.
//  Copyright © 2018 ccanv. All rights reserved.
//

import WatchKit
import Foundation


class ObjetoInterface: WKInterfaceController {
    
    
    @IBOutlet weak var nombreObjeto: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        let objeto = context as! String
        self.nombreObjeto.setText(objeto)
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
